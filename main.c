#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

void menu(struct Client **head);

int main (){
    struct Client *head;
        head = (struct Client*)malloc(sizeof(struct Client));
        head = NULL;
        char surname[20]="";
    int menu;
    int x=0;

        printf("Hello, what do you want to do?\n");
        printf("1. Add clients to the list\n 2. Delete clients from the list\n3. Show the list\n4. Show the number of the clients \n5. EXIT\n");
        scanf("%d",&menu);
        switch (menu)
        {
        case 1:
        {
            add_client(&head);
            break;
        }
        case 2:
        {
            char sname [20];
            printf("Enter surname to delete:");
            scanf("%c",&sname);
            pop_by_surname(&head,sname);
            break;
        }
        case 3:
        {
            list_size(head);
            break;
        }
        case 4:
        {
            show_list(head);
            break;
        }
        case 5: exit(0);
        default: printf("ERROR 404");
        break;
    }


return 0;
}

void add_client(struct Client **head)
{
    int l=0, i=0,d=0;
    char surname[20]="";
    printf("How many clients do you want to add?");
    scanf("%d",&l);

    printf("You want to add on the top of the list (1), \n at the end of the list (2), \nby the index(3)\n");
    scanf("%d",&i);
    do{
    printf("Enter surname:");
    scanf("%c",&surname);

    if (i==1) push_front(&head,surname);
    else if (i==2) push_back(&head,surname);
    else if (i==3){
        int index=0;
        printf("Enter index:\n");
        scanf("%d",&index);
        push_by_index(&head,surname,index);
    }
    d++;
    }while (d<l);
    return;
}
